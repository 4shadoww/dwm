#!/usr/bin/env bash

export DISPLAY=:0
export DBUS_SESSION_BUS_ADDRESS=unix:path=/run/user/1000/bus

WALLPAPER_TO_BE_SET=$(find /home/shadoww/Pictures/wallpapers | grep -E "\.(jpe?g|png)$" | sort -R | sed -e '$!d')
nitrogen $WALLPAPER_TO_BE_SET --set-auto --save --head=0
nitrogen $WALLPAPER_TO_BE_SET --set-auto --save --head=1
